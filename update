#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from collections import defaultdict

import gzip
import string
import time
import fileinput
import os

import requests
import json
import base64

#folder containing public data (html website)
WEB_ROOT="./"

#folder containing private data(anticaptcha key, cookies)
KEYS_ROOT="./../"

''' UPDATE 17/06 23:00 -> captchas ajoutes: on a besoin d'utiliser anti captcha.
    https://anti-captcha.com
    Mettez la cle dans le fichier ANTICAPTCHA_KEY_FILE
'''
ANTICAPTCHA_KEY_FILE=KEYS_ROOT+"key.dat"
ANTICAPTCHA_KEY = ""
with open(ANTICAPTCHA_KEY_FILE) as ac_file:
    ANTICAPTCHA_KEY = ac_file.readlines()[0].rstrip("\r\n")

''' UPDATE 17/06 20:00 -> google captcha ajoute (je croyais le gvt anti-GAFAM? mais que se passe-t-il?)
    une fois le captcha valide, il vous ajoute des cookies, les charger ici (j'en ai 5 apparement, peut
    etre certains sont inutiles)

	exemple fictif. Copier les bonnes clefs-valeurs dans le fichier COOKIES_FILE :
	incap_ses_947_2147127:NB0dVrcXCTLgsuFPTR1tAZDRC20BBBBB90doQ9EMO3A17bibEFCZhS==
	nlbi_2273138:HIgYKgijXxVPOcHYqtI6oACAABB01KNiVcvDJ7KvzAn7AONK
	visid_incap_2173329:oEQt5bXrUlS57PldhfLbQZHRC10AAAAAQkIPABCDABCAOv+MAVL+vokisqxxwcZWBzOpqHANnUoj
	A10_Insert-12472:ABAEOJBKFABC
	PHPSESSID:bcofoi71acfsa4mph2jvg4d7c7

    je ne mets pas mon véritable cookie dans ce script open-source pour proteger ma vie privee. Utilisez votre propre cookies.

    NOTE: cookie? et la RGPD dans tout ca? l'europe devrait mettre une amende a ce site du gouvernement... a moins qu'il soit au dessus de ca.
'''
COOKIES_FILE=KEYS_ROOT+"cookies.dat"
COOKIES = {}
with open(COOKIES_FILE) as cookie:
    cookies = cookie.readlines()
    for c in cookies:
        data = c.split(":")
        COOKIES[data[0]] = data[1].rstrip("\r\n")



def display_html_num(num):
    '''Formatte un chiffre avec des 'thin space' entre milliers/millions/...'''
    return '{:,}'.format(num).replace(',', "&thinsp;")

def trim_eol(data):
    return data.replace("\r","").replace("\n","")

def clean_data(ville):
    '''Nettoie une ville du HTML vers notre format CSV'''
    return ville.replace("&#039;", "'")

def get_ville_key(VILLES, ville):
    '''Pour un dictionnaire de localite VILLES, trouve une cle possible correspondant a ville'''
    if ville in VILLES: #if it's a key, just return the key
        return ville

    for k, v in VILLES.iteritems(): #sinon la ville est dans la cle (partial match qui ne fait pas full match), retourner la cle entiere
        if ville in k:
            return k

    return "autres" #sinon echec de trouver la ville

def parse_data(VILLES, data):
    '''Parse le contenu de data et MAJ VILLES'''
    data = data.split("<td  width=\"33%\">")[1:]
    for i,d in enumerate(data):
        if i%2==1:
            ville = clean_data(d[:d.find("</td>")])
            ville_key = get_ville_key(VILLES, ville)
            VILLES[ville_key][2] += 1


    return VILLES


def department_reduce(localites):
    '''Prends en argument un dictionnaire localite ~> nombre de votants et retourne un dictionaire numero du departement ~> nombre de votants'''
    out = {}
    for v in localites.values():
        if v[0] == "00000": #ignorer villes inconnues
            continue

        if not isDigit(v[0][0]):
            continue

        CODE = v[0][0:2]
        if v[0].startswith("971"): #guadeloupe
            CODE = "GP"
        elif v[0].startswith("972"): #martinique
            CODE = "MQ"
        elif v[0].startswith("973"): #guyanne
            CODE = "GF"
        elif v[0].startswith("974"): #reunion
            CODE = "RE"
        elif v[0].startswith("976"): #mayotte
            CODE = "YT"

        if CODE in out:
            out[CODE] += v[2]
        else:
            out[CODE] = v[2]

    return out


def url_dll(url):
    result = ""
    while len(result) == 0:
        try: #URL bug par moment: OpenSSL.SSL.ZeroReturnError
            rep = requests.get(url, cookies=COOKIES)
            while len(rep.content) == 0:
                time.sleep(0.5)
                rep = requests.get(url)

            result = rep.content
        except Exception as e:
            print("Exeception for url '"+url+"': "+str(e))
            time.sleep(1)

    return result

def extract_data_between(data, start, end):
    data = data[data.find(start)+len(start):]
    data = data[:data.find(end)]
    return data

def check_captcha_task(taskId, sleepTime):
    '''Recupere une tache de reconnaissance de captcha'''
    result = requests.post("https://api.anti-captcha.com/getTaskResult", json={"clientKey":ANTICAPTCHA_KEY,"taskId":taskId})
    try:
        jresult = json.loads(result.content)
        while jresult["status"] != "ready":
            if jresult["status"] == "processing":
                time.sleep(sleepTime)
                result = requests.post("https://api.anti-captcha.com/getTaskResult", json={"clientKey":ANTICAPTCHA_KEY,"taskId":taskId})
                jresult = json.loads(result.content)
            else:
                raise Exception("error while processing microtask: "+result.content)
    except Exception as e:
        raise Exception("error while processing microtask: "+result.content+" (exception: "+str(e)+")")

    return jresult

def make_captcha_task(task_data):
    return requests.post("http://api.anti-captcha.com/createTask", json={"clientKey":ANTICAPTCHA_KEY,"task":task_data})

def get_captcha(image):
    '''Lance une tache de reconnaissance de captcha'''
    ret = ""
    while len(ret) == 0:
        try:
            task = make_captcha_task({"type":"ImageToTextTask","body":"\""+image+"\"","case":True})
            jtask = json.loads(task.content)
            if jtask["errorId"] != 0:
                raise Exception("could not run microtask: "+task.content)

            ret = check_captcha_task(jtask["taskId"], 1)["solution"]["text"]
        except Exception as e:
            time.sleep(1)
            print("Erreur en capturant le captcha: "+str(e)+", retryage...")

    return ret

def isDigit(c):
    return c>='0' and c<='9'

def get_num_pages(data):
    '''Extrait le nombre de page (pagination) pour un element IJ donne'''
    idx = 0
    nums = []
    while idx != -1:
        idx = data.find("page=", idx+1)
        numIdxStart = idx+len("page=")
        numIdx = numIdxStart
        while isDigit(data[numIdx]):
            numIdx += 1

        num = data[numIdxStart:numIdx]
        if len(num) != 0:
            nums.append(int(num))

    ret = list(dict.fromkeys(nums)) #enleve les doublons

    if len(ret) == 0:
        return ret

    if ret[len(ret)-1]>=10:
        return range(2,ret[len(ret)-1]+1)

    return ret

def bypass_captcha(url,idx,I,J):
    '''Dll IJ page idx en bypassant le captcha'''
    current_url = url + "/" + I + "/" + I + J
    if idx != 1:
        current_url +=  "?page="+str(idx)

    data = trim_eol(url_dll(current_url))
    if "Request unsuccessful. Incapsula incident ID" in data or "https://www.google.com/recaptcha/api2/bframe" in data: #need to generate a new set of cookie
        print(COOKIES)
        raise Exception("Incapsula cookie stop")

    ''' #essaye de MAJ automatiquement les cookies mais ca ne semble pas possible? si quelqu'un a une idee....
        iframe = extract_data_between(data, "iframe src=\"", "\" frameborder=0")
        website_url = "https://www.referendum.interieur.gouv.fr/"+iframe
        iframe_data = trim_eol(url_dll(website_url))
        data_sitekey = extract_data_between(iframe_data, "data-sitekey=\"", "\" data-callback=\"")
        task = make_captcha_task({"type":"NoCaptchaTaskProxyless","websiteKey":data_sitekey,"websiteURL":website_url.replace("/","\\/")})
        print(task.content)

        jtask = json.loads(task.content)
        if jtask["errorId"] != 0:
            raise Exception("could not run microtask: "+task.content)

        print(check_captcha_task(jtask["taskId"]))

        print("Need to refresh cookies...")
        os.Exit(1)
    '''

    orig_data = data
    while "Captcha" in data and "Saisir les caract" in data:
        try:
            token = extract_data_between(data, "name=\"form[_token]\" class=\" form-control\" value=\"", "Retour")
            token = extract_data_between(token, "", "\" />")
            print("\tGot captcha token="+token)
            image = requests.get("https://www.referendum.interieur.gouv.fr/bundles/ripconsultation/securimage/securimage_show.php", cookies=COOKIES, stream=True)
            CAPTCHA_VAL = get_captcha(base64.b64encode(image.content))
            print("\t"+str(idx)+","+I+","+J+","+"CAPTCHA="+CAPTCHA_VAL)

            rep = requests.post(current_url, cookies=COOKIES, data={"form[captcha]":CAPTCHA_VAL,"form[_token]":token})
            data = trim_eol(rep.content)
        except Exception as e:
            print("Erreur lors de la validation/recuperation du captche: "+str(e)+", retry apres 1s...")
            data = orig_data
            time.sleep(1)


    return data

def full_check_data(url):
    '''Verifie les MAJ sur URL et retourne une hashmap: localite ~> nombre de votants'''

    VILLES = {} #Liste des villes avec le nombre total d'inscrit dll sur le site de l'INSEE + arrondissements Paris/Lyon/Marseille (2017 premier tour)
    VILLES["autres"] = ["00000",1,0]
    with open(WEB_ROOT+"villes_insee.dat", "r") as villes:
        for ville in villes:
            data = ville.split(";")
            if len(data)!=3:
                print(ville, data)
                raise Exception("invalid CSV insee")

            nb_inscrit = data[2].rstrip("\n")
            if nb_inscrit == "N/A":
                continue

            VILLES[data[1]] = [data[0],int(data[2].rstrip("\n")),0]

    for I in "A":#string.ascii_uppercase:
        for J in "A":#string.ascii_uppercase:
            print("Fetching "+I+J+"...")
            data = bypass_captcha(url, 1, I, J)

            if "Aucun soutien n'est accept" in data:
                continue

            parse_data(VILLES, data) #parse current data

            pages = get_num_pages(data) #get other pages
            for p in pages:
                print("Fetching "+I+J+"."+str(p)+"...")
                data = bypass_captcha(url, p, I, J)
                parse_data(VILLES, data)

    return VILLES

def archive_write(dest, localites):
    '''Sauvegarde le dictionnaire localites au format csv dans le fichier dest'''
    nombre_autres = localites["autres"][2]
    for ville, v in localites.items():
        code_insee = v[0]
        total_inscrit = v[1]
        soutien = v[2]
        if code_insee == "00000": #ignorer le code autres
            continue

        if soutien > total_inscrit:
            print("ATTENTION: plus de soutiens que d'inscrits pour: ",ville,v," probablement get_ville_key a corriger; deplace cette ville dans autres 00000")
            nombre_autres += soutien
            continue

        dest.write(code_insee+";"+ville+";"+str(total_inscrit)+";"+str(soutien)+"\n")

    #concatene autres; ici total_inscrit = soutien puisque total_inscrit est faux (pas de donnees insee, ville fictive)
    dest.write("00000;autres;"+str(localites["autres"][2])+";"+str(localites["autres"][2])+"\n")

def generate_result(localites):
    '''Prends une hashmap localite en entree, retourne {nombre de votants, la string a remplacer dans index.html}'''
    required = 4717396
    count = 0
    for v in localites.values():
        count += v[2]

    out = display_html_num(count)+"/"+display_html_num(required)+" ("
    percentage = 100*float(count)/float(required)
    if count == 0:
        out += "0"
    elif count <= required/100: #moins de un pourcent
        out += "{:1.5f}".format(percentage)
    else:
        out += "{:2.3f}".format(percentage)
    out += "%) <a href=\\\"https://www.adprip.fr/\\\"><img src=\\\"https://upload.wikimedia.org/wikipedia/commons/6/64/Icon_External_Link.png\\\"></a>"

    return count, out

def update_data(localites):
    '''MAJ les donnees sur le serveur en fonction de la hashmap localite '''
    COUNT, OUT = generate_result(localites)
    print("Index updated to: "+OUT)

    cpath = WEB_ROOT+"js/counter.js"
    if os.path.exists(cpath):
        os.remove(cpath)
    with open(cpath, "a") as counter:
        counter.write("var MAIN_COUNT = \""+str(OUT)+"\";\n")

    for line in fileinput.input(WEB_ROOT+"index.html", inplace=True):
       if line.startswith("<!--CRON -->"):
           print("<!--CRON -->"+OUT)
       else:
           print(line.rstrip('\r\n'))

    if COUNT!=0:
        TS = str(int(time.time()))
        with open(WEB_ROOT+"history.dat", "a") as history:
            history.write(TS+";"+str(COUNT)+"\n")

        with gzip.open(WEB_ROOT+"archive/"+TS+".dat.gz", "w") as archive:
            archive_write(archive, localites)

        #latest geo data
        latest = WEB_ROOT+"archive/latest"
        if os.path.exists(latest):
            os.remove(latest)
        with open(latest, "w") as ldata:
            archive_write(ldata, localites)

        #js map
        map_path = WEB_ROOT+"js/vote_data.js"
        if os.path.exists(map_path):
            os.remove(map_path)
        with open(map_path, "w") as jsmap:
            jsmap.write("var vote = {\n")
            for k, v in department_reduce(localites).items():
                jsmap.write("\t\"FR-"+k+"\": "+str(v)+",\n")
            jsmap.write("};\n")


if __name__ == "__main__":
    LOCALITES = full_check_data("https://www.referendum.interieur.gouv.fr/consultation_publique/8")
    update_data(LOCALITES)
